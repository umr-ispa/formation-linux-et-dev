# Introduction
Ce projet vise à élaborer un support pour un atelier d'introduction à l'usage de Linux et aux bonnes pratiques de développement.
Le support, volontairement spartiate, pose juste les notions clés qui seront développées oralement lors de la présentation et des échanges.

# Caractéristiques
* Ce support de présentation est basé sur un document écrit en Latex utilisant le template Beamer.
* Il se compile avec la commande "make"
* Le CI/CD sert à valider la compilation mais ne génère pas le rendu dans l'état actuel de sa configuration